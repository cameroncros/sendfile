RM := rm -rf

# All of the sources participating in the build are defined here
-include sources.mk
-include tests/subdir.mk
-include src/subdir.mk
-include subdir.mk
-include objects.mk

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(strip $(C++_DEPS)),)
-include $(C++_DEPS)
endif
ifneq ($(strip $(C_DEPS)),)
-include $(C_DEPS)
endif
ifneq ($(strip $(CC_DEPS)),)
-include $(CC_DEPS)
endif
ifneq ($(strip $(CPP_DEPS)),)
-include $(CPP_DEPS)
endif
ifneq ($(strip $(CXX_DEPS)),)
-include $(CXX_DEPS)
endif
ifneq ($(strip $(C_UPPER_DEPS)),)
-include $(C_UPPER_DEPS)
endif
endif

-include ../makefile.defs

# Add inputs and outputs from these tool invocations to the build variables 

# All Target
all: libsendfile.so tests

# Tool invocations
libsendfile.so: $(OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C++ Linker'
	g++ -shared -o "$(BUILD_DIR)/libsendfile.so" -fPIC -g $(OBJS) $(LIBS) 
	@echo 'Finished building target: $@'
	@echo ' '

tests: libsendfile.so $(TEST_OBJS)
	@echo 'Building target: $@'
	@echo 'Invoking: GCC C++ Linker'
	g++ -o "$(BUILD_DIR)/sendfile_test" -g $(TEST_OBJS) $(LIBS) -lcheck
	@echo 'Finished building target: $@'
	@echo ' '
# Other Targets
clean:
	-$(RM) $(TEST_OBJS)$(TEST_C_OBJS)$(TEST_C_DEPS)$(OBJS)$(C++_DEPS)$(C_DEPS)$(CC_DEPS)$(LIBRARIES)$(CPP_DEPS)$(CXX_DEPS)$(C_UPPER_DEPS) $(BUILD_DIR)/libsendfile.so $(BUILD_DIR)/sendfile_test
	-@echo ' '


.PHONY: all tests
.SECONDARY:


