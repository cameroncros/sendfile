/*
 * receivefile.h
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */

#include "main.h"
#include <stdbool.h>

#ifndef RECEIVEFILE_H_
#define RECEIVEFILE_H_

bool receiveFile(Host host);
bool receiveFile_Direct(Host host);
bool receiveFile_Diff(Host host);


#endif /* RECEIVEFILE_H_ */
