/*
 * sendfile.c
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */

#include "sendfile.h"
#include "socketfunctions.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>



bool sendFile(Type type, char *sourcename, char *destname, Host host) {
	bool output;
	if (setupHostSender(&host) == false) {
		return false;
	}
	FileData *fd = readFile(sourcename);

	sendData(host, DESTNAME, destname);
	sendData(host, HASH, fd->hash);
	sendData(host, SIZE, &(fd->size));
	switch (type) {
	case DIRECT:
		sendFile_Direct(fd, host);
		break;
	case DIFF:
		sendFile_Diff(fd, host);
		break;
	default:
		break;
	}
	receiveData(host, CONFIRMATION, &output);
	return output;
}
bool sendFile_Auto(FileData *fd, char *destname, Host host) {
	return false;
}

void sendFile_Direct(FileData *fd, Host host) {
	sendData(host, FILEDATA, fd->data);
}
void sendFile_Diff(FileData *fd, Host host) { //TODO
	while (!EOF) {
		sendData(host, PARTIALHASH, "asdf");
		sendData(host, PARTIALFILE, "asdfa");
	}
}

FileData *readFile(char *sourcename) {
	FileData *fd = (FileData*) malloc(sizeof(FileData));
	fd->fp = fopen(sourcename, "rb");
	int filedescriptor = fileno(fd->fp);
	if (filedescriptor == -1) {
		perror("Error opening file for reading");
		exit(EXIT_FAILURE);
	}
	fseek(fd->fp, 0L, SEEK_END);
	fd->size = ftell(fd->fp);
	fseek(fd->fp, 0L, SEEK_SET);

	fd->data = (char *)mmap(0, fd->size, PROT_READ, MAP_SHARED, filedescriptor, 0);
	if (fd->data == MAP_FAILED) {
		fclose(fd->fp);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	return fd;
}

bool closeFile(FileData *fd) {
	/* Don't forget to free the mmapped memory
	 */
	if (munmap(fd->data, fd->size) == -1) {
		perror("Error un-mmapping the file");
		/* Decide here whether to close(fd) and exit() or not. Depends... */
	}

	/* Un-mmaping doesn't close the file, so we still need to do that.
	 */
	fclose(fd->fp);
	return true;
}


