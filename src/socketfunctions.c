/*
 * socketfunctions.c
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */
#include "socketfunctions.h"
#include "main.h"
#include <stddef.h>
#include <sys/socket.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <netdb.h>
#include <unistd.h>

bool setupHostReceiver(Host *host) {
	int out, yes=1;
	struct addrinfo hints, *servinfo = NULL, *p= NULL;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	printf("Listening on: %s\n",host->port);

	if ((out = getaddrinfo(NULL, host->port, &hints, &servinfo)) != 0) {
		printf("(%s:%i) getaddrinfo: %s\n", __FILE__, __LINE__, gai_strerror(out));
		return false;
	}

	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((host->socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			close(host->socket);
			host->socket = 0;
			printf("(%s:%i) socket: %s\n", __FILE__, __LINE__, strerror(errno));
		}
		if (setsockopt(host->socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
			printf("(%s:%i) socket: %s\n", __FILE__, __LINE__, strerror(errno));
			return false;
		}
		if (bind(host->socket, p->ai_addr, p->ai_addrlen) == -1) {
			close(host->socket);
			host->socket = 0;
			printf("(%s:%i) socket: %s\n", __FILE__, __LINE__, strerror(errno));
			return false;
		} else {
			break;
		}
	}
	return true;

}

bool setupHostSender(Host *host) {
	int out;
	struct addrinfo hints, *servinfo = NULL, *p= NULL;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	printf("Connecting to: %s:%s\n",host->addr,host->port);


	if ((out = getaddrinfo(host->addr, host->port, &hints, &servinfo)) != 0) {
		printf("(%s:%i) getaddrinfo: %s\n", __FILE__, __LINE__, gai_strerror(out));
		return false;
	}
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((host->socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			close(host->socket);
			host->socket = 0;
			printf("(%s:%i) socket: %s\n", __FILE__, __LINE__, strerror(errno));
		}
		if (connect(host->socket, p->ai_addr, p->ai_addrlen) == -1) {
			close(host->socket);
			host->socket = 0;
			printf("(%s:%i) socket: %s\n", __FILE__, __LINE__, strerror(errno));
		} else {
			break;
		}
	}
	freeaddrinfo(servinfo);
	if (p == NULL) {
		if (host->socket) {
			close(host->socket);
			host->socket = 0;
		}

		printf("(%s:%i) Failed Connection: %s\n", __FILE__, __LINE__, strerror(errno));
		return false;
	}
	return true;


}

bool setupHostSenderSSL(Host *host) {
	int out;
	SSL *secure = NULL;
	SSL_CTX *sslContext = NULL;
	if (setupHostSender(host) == false) {
		return false;
	}

	//finished creating a socket, now add the ssl part(curtesy of: http://savetheions.com/2010/01/16/quickly-using-openssl-in-c/)
	if ((sslContext = SSL_CTX_new(SSLv23_client_method ())) == NULL) {
		printf("(%s:%i) SSL: %s\n", __FILE__, __LINE__, ERR_error_string(errno, NULL));
		return false;
	}
	if ((secure = SSL_new(sslContext)) == NULL) {
		printf("(%s:%i) SSL: %s\n", __FILE__, __LINE__, ERR_error_string(errno, NULL));
		return false;
	}
	if (!(out = SSL_set_fd(secure, host->socket))) {
		printf("(%s:%i) SSL: %s\n", __FILE__, __LINE__, ERR_error_string(errno, NULL));
		return false;
	}
	if ((out = SSL_connect (secure)) != 1) {
		printf("(%s:%i) SSL: %s\n", __FILE__, __LINE__, ERR_error_string(errno, NULL));
		return false;
	}
	printf("Made a connection\n");
	return false;
}

bool receiveDataSSL(Host host, int size, void *data) {
	if (SSL_read(host.secure, &size, sizeof(int)) == -1) {
		printf("(%s:%i) recv: %s\n", __FILE__, __LINE__, strerror(errno));
		return false;
	}
	int total = 0, recvd;
	while (total < size) {
		if ((recvd = SSL_read(host.secure, *(&data+total), size-total)) != -1) {
			total += recvd;
		} else {
			return false;
		}
	}
	return true;
}

bool sendDataSSL(Host host, int size, void *data) {
	int total = 0, sent;
	while (total != sizeof(int)) {
		if ((sent = SSL_write(host.secure, &size, sizeof(int)-total)) != -1) {
			total += sent;
		} else {
			printf("(%s:%i) send: %s\n", __FILE__, __LINE__, strerror(errno));
			return false;
		}
	}
	total = 0;
	while (total != size) {
		if ((sent = SSL_write(host.secure, data, size-total)) != -1) {
			total += sent;
		} else {
			printf("(%s:%i) send: %s\n", __FILE__, __LINE__, strerror(errno));
			return false;
		}
	}
	return true;
}

bool receiveData(Host host, int size, void *data) {
	if (SSL_read(host.secure, &size, sizeof(int)) == -1) {
		printf("(%s:%i) recv: %s\n", __FILE__, __LINE__, strerror(errno));
		return false;
	}
	int total = 0, recvd;
	while (total < size) {
		if ((recvd = read(host.socket, *(&data+total), size-total)) != -1) {
			total += recvd;
		} else {
			return false;
		}
	}
	return true;
}

bool sendData(Host host, int size, const void *data) {
	int total = 0, sent;
	while (total != sizeof(int)) {
		if ((sent = send(host.socket, &size, sizeof(int)-total, 0)) != -1) {
			total += sent;
		} else {
			printf("(%s:%i) send: %s\n", __FILE__, __LINE__, strerror(errno));
			return false;
		}
	}
	total = 0;
	while (total != size) {
		if ((sent = send(host.socket, data, size-total, 0)) != -1) {
			total += sent;
		} else {
			printf("(%s:%i) send: %s\n", __FILE__, __LINE__, strerror(errno));
			return false;
		}
	}
	return true;
}
