/*
 * sendfile.h
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */

#include "main.h"
#include <stdbool.h>

#ifndef SENDFILE_H_
#define SENDFILE_H_





bool sendFile(Type type, char *sourcename, char *destname, Host host);
bool sendFile_auto(char *sourcename, char *destname, Host host);

void sendFile_Direct(FileData *fd, Host host);
void sendFile_Diff(FileData *fd, Host host);

FileData *readFile(char *sourcename);



#endif /* SENDFILE_H_ */
