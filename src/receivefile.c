/*
 * receivefile.c
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */

#include "receivefile.h"
#include "socketfunctions.h"
#include <stdbool.h>

bool receiveFile_Direct(Host host);
bool receiveFile_Diff(Host host);

bool receiveFile(Host host) {
	Type type;
	receiveData(host, TYPE, &type);
	switch (type) {
	case NEGOTIATE:
		return false;
	case DIRECT:
		return receiveFile_Direct(host);
	case DIFF:
		return receiveFile_Direct(host);
	}
 return false;
}

bool receiveFile_Direct(Host host) {
	if (setupHostReceiver(&host) == false) {
		return false;
	}
	return false;
}

bool receiveFile_Diff(Host host) {
	if (setupHostReceiver(&host) == false) {
		return false;
	}
	return false;
}
