/*
 * main.h
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stddef.h>
#include <stdbool.h>
#include <openssl/ssl.h>


#define VERSION 0.0.1
#define PROTOVER 1

typedef enum {
	NEGOTIATE,
	DIRECT,
	DIFF,
} Type;

typedef enum {
	TYPE         = 1,
	DESTNAME     = 2,
	FILEDATA     = 3,
	HASH         = 4,
	SIZE         = 5,
	CONFIRMATION = 6,
	PARTIALHASH  = 7,
	PARTIALFILE  = 8
} PacketType;

/**
 * addr and port can be empty if socket is a valid socket.
 * socket should be left alone. if ssl==false, then the socket
 * is unencrypted, otherwise it will be encrypted
 */
typedef struct {
	bool connected;
	char *addr;
	char *port;
	bool ssl;
	int socket;
	SSL *secure;
} Host;

typedef struct {
	FILE *fp;
	char *data;
	int size;
	char *hash;
} FileData;



#endif /* MAIN_H_ */
