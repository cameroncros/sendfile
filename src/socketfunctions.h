/*
 * socketfunctions.h
 *
 *  Created on: 22/01/2014
 *      Author: cameron
 */

#ifndef SOCKETFUNCTIONS_H_
#define SOCKETFUNCTIONS_H_

#include "main.h"
#include <stdbool.h>

bool setupHostReceiver(Host *host);
bool setupHostSender(Host *host);

bool setupHostReceiverSSL(Host *host);
bool setupHostSenderSSL(Host *host);

bool receiveData(Host host, int size, const void *data);
bool sendData(Host host, int size, const void *data);

bool receiveDataSSL(Host host, int size, const void *data);
bool sendDataSSL(Host host, int size, const void *data);

#endif /* SOCKETFUNCTIONS_H_ */
