#include <check.h>

#include <stdio.h>
START_TEST (test_name)
{
	ck_assert_int_eq (5, 5);
	ck_assert_str_eq ("AUD", "USD");
	printf("Hello World");
}
END_TEST

Suite *
money_suite (void)
{
	Suite *s = suite_create ("Send");

	/* Core test case */
	TCase *tc_core = tcase_create ("Core");
	tcase_add_test (tc_core, test_name);
	suite_add_tcase (s, tc_core);

	return s;
}

int
main (void)
{
	int number_failed;
	Suite *s = money_suite ();
	SRunner *sr = srunner_create (s);
	srunner_run_all (sr, CK_NORMAL);
	number_failed = srunner_ntests_failed (sr);
	srunner_free (sr);
	return number_failed;
}
